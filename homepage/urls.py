from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('story1/', views.story1, name='story1'),
    path('profile/', views.index, name='profile'),
    path('trackrecord/', views.trackrecord, name='trackrecord'),
    path('gallery/', views.gallery, name='gallery'),
]