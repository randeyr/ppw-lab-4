from django.shortcuts import render

def home(request):
    return render(request, 'homepage.html')

def story1(request):
    return render(request, 'story1.html')

def index(request):
    return render(request, 'index.html')

def trackrecord(request):
    return render(request, 'trackrecord.html')

def gallery(request):
    return render(request, 'gallery.html')